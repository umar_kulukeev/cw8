<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('umar')
            ->setRoles(["ROLE_ADMIN"])
            ->setEnabled(true)
            ->setEmail('uumar686@gmail.com')
            ->setAddress('Bishkek')
            ->setName('Umar')
            ->setPhone('0705007507');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '1234');
        $user->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
