<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Institution;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadInstitutionData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $institution = new Institution();
        $institution
            ->setName('Фаиза')
            ->setAvatar('faiza.png')
            ->setDescription('национальная и восточная кухня');

        $manager->persist($institution);

        $manager->flush();
    }
}
