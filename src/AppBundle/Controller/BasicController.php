<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BasicController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET","HEAD"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $institutions = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', array(
            'Institution' => $institutions
        ));
    }

    /**
     * @Route("/institutions/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(int $id) {
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        return $this->render('AppBundle:Basic:details.html.twig', array(
            'Institution' => $institution
        ));
    }

}
